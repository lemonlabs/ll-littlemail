var io = require('socket.io-client')
var socket = io.connect('lemonlabs.lt', {
    port: 3000
});

var sys = require('sys')
var exec = require('child_process').exec;

socket.on('connect', function () {
  console.log("connected");

  socket.on('ping', function (msg) {
    console.log('got ping: ' + msg)
    socket.emit('pong')
  });
});

socket.on('connecting',      function () { console.log('connecting'); });
socket.on('connect_failed',  function () { console.log('connect_failed'); });
socket.on('connect_error',   function (e) { console.log(e); });
socket.on('error',           function (e) { console.log(e); });
socket.on('disconnect',      function () { console.log('disconnect'); });
socket.on('reconnect',       function (count) { console.log('reconnect ' + count ); });
socket.on('reconnect_error',  function (e) { console.log('reconnect_error ' + e); });
socket.on('reconnect_failed', function () { console.log('reconnect_failed'); });
