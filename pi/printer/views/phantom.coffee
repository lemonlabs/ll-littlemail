page = require('webpage').create()
page.viewportSize = { width: 488, height: 3000 };
fs = require('fs')
system = require('system')
{spawn, execFile} = require('child_process')

page.onConsoleMessage = (msg, lineNum, sourceId) ->
  console.log "CONSOLE: #{ msg } (from line ##{ lineNum } in #{ sourceId })"

renderPage = ->
  renderHeight = page.evaluate -> renderHeight()
  page.clipRect =
    top: 30
    left: 52
    width: 384
    height: renderHeight
  page.render "output.png"

objectType = system.args[1]
objectDataString = system.args[2]
objectData = JSON.parse(objectDataString)

page.open fs.absolute('views/' + objectType + '/page.html'), (status) ->
  if status isnt 'success'
    console.log 'Unable to access the network!'
  else
    page.evaluate ((data) -> setData(data)), objectData
    console.log 'Rendering...'
    renderPage()
    console.log 'Printing...'
    execFile "ls", ["-la"], null, (err, stdout, stderr) ->
      console.log "execFileSTDOUT:", JSON.stringify stdout
      console.log "execFileSTDERR:", JSON.stringify stderr
    execFile "python", ["/home/pi/printer/print.py", "/home/pi/printer/output.png"], null, (err, stdout, stderr) ->
      console.log "execFileSTDOUT:", JSON.stringify stdout
      console.log "execFileSTDERR:", JSON.stringify stderr
  phantom.exit()
