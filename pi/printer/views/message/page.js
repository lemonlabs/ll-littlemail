var renderHeight, setData;

renderHeight = function() {
  return $('#render').outerHeight();
};

setData = function(data) {
  $('.body .meta .from').html(data['from']);
  $('.body .body').html(data['body'].replace(/\n/g, '<br />').replace(/\s{2,}/g, ' '));
};
