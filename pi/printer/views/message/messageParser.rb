class MessageParser
  attr_accessor :body, :from

  def initialize body, from
    @body = body
    @from = from
  end

  def parse
    data = {}
    data['body'] = @body
    data['from'] = @from
    data
  end
end
