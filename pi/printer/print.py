#!/usr/bin/python

from Adafruit_Thermal import *
import sys
import Image

printer = Adafruit_Thermal("/dev/ttyAMA0", 19200, timeout=5)

printer.printImage(Image.open(sys.argv[1]), True)
printer.println("")
printer.println("")
