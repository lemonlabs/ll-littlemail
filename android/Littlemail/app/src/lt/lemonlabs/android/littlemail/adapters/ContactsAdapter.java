package lt.lemonlabs.android.littlemail.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;
import lt.lemonlabs.android.littlemail.entities.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 21:39
 */
public class ContactsAdapter extends BaseAdapter {

    private static final int[] coverIds = new int[]{R.drawable.friends_avatar_cover_1, R.drawable.friends_avatar_cover_2, R.drawable.friends_avatar_cover_3, R.drawable.friends_avatar_cover_4};
    private static final String TAG = "ContactAdapter";

    private Context context;

    private ArrayList<Contact> items;

    private ImageLoader imageLoader;

    public ContactsAdapter(Context context) {
        this.context = context;
        items = new ArrayList<Contact>();
        imageLoader = ImageLoader.getInstance();
    }

    public void setItems(List<Contact> contacts) {
        items.clear();
        items.addAll(contacts);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Contact getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.contacts_list_item, null);

            final ViewHolder holder = new ViewHolder();
            holder.container = convertView.findViewById(R.id.contact_list_container);
            holder.avatar = (ImageView) convertView.findViewById(R.id.contact_list_avatar);
            holder.cover = (ImageView) convertView.findViewById(R.id.contact_list_avatar_cover);
            holder.name = (TextView) convertView.findViewById(R.id.contact_list_name);
            holder.overlay = convertView.findViewById(R.id.contact_list_overlay);

            convertView.setTag(holder);
        }

        final ViewHolder holder = (ViewHolder) convertView.getTag();

        final Contact contact = getItem(position);

        // Assign values
        holder.name.setText(contact.name);
        holder.cover.setImageResource(coverIds[position % 4]);

        holder.overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Contact contact = getItem((Integer) holder.overlay.getTag());
                ((LittlemailActivity)context).goToMessage(contact);
            }
        });

        holder.overlay.setTag(position);

        imageLoader.loadImage(contact.url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (loadedImage != null)
                    holder.avatar.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }
        });


        return convertView;
    }


    static class ViewHolder {
        View container;
        TextView name;
        ImageView cover;
        ImageView avatar;
        View overlay;
    }
}
