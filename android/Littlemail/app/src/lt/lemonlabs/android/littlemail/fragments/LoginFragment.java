package lt.lemonlabs.android.littlemail.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.loopj.android.http.JsonHttpResponseHandler;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;
import lt.lemonlabs.android.littlemail.common.*;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 16:19
 */
public class LoginFragment extends LittlemailFragment {

    public static final String TAG = "LoginFragment";

    private UiLifecycleHelper uiHelper;

    private LoginButton facebookLoginBtn;
    private Button learnMoreBtn;

    private Dialog dialog;
    private boolean requestLock;

    public static LittlemailFragment newInstance() {
        LoginFragment f = new LoginFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.login, viewGroup, false);

        facebookLoginBtn = (LoginButton) v.findViewById(R.id.login_facebook_button);
        facebookLoginBtn.setFragment(this);

        learnMoreBtn = (Button) v.findViewById(R.id.login_learn_more_button);
        learnMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.web_url)));
                startActivity(browserIntent);
            }
        });

        initActionBar();

        uiHelper = new UiLifecycleHelper(getActivity(), facebookCallback);
        uiHelper.onCreate(savedInstanceState);
        return v;
    }


    private void onFacebookSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            final String token = session.getAccessToken();
            if (token.length() != 0 && !requestLock) {
                requestLock = true;
                final AppSettings appSettings = AppSettings.getInstance(getActivity());
                appSettings.setFacebookToken(token);

                dialog = DialogHelper.showTranslucentProgressDialog(getActivity(), false, null);

                final JSONObject json = new JSONObject();
                try {
                    json.put("facebook_token", token);
                    new Api.OAuth(getActivity(), json).call(getActivity(), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, JSONObject response) {
                            L.d(TAG, response);
                            requestLock = false;

                            final String appToken = response.optString("token", "");

                            if (appToken.length() != 0) {
                                appSettings.setAppToken(appToken);
                                ((LittlemailActivity)getActivity()).goToContacts();
                            } else {
                                Toast.makeText(getActivity(), "App token is empty :(", Toast.LENGTH_SHORT).show();
                            }

                            dialog.dismiss();
                        }

                        @Override
                        public void onFailure(int statusCode, JSONObject errorResponse, Throwable error) {
                            L.e(TAG, errorResponse);
                            requestLock = false;

                            dialog.dismiss();
                            ((LittlemailActivity)getActivity()).goToContacts();
                        }
                    });

                } catch (JSONException e) {

                }


            }
        } else if (state.isClosed()) {
            L.d(TAG, "Logged out...");
        }
    }

    private Session.StatusCallback facebookCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onFacebookSessionStateChange(session, state, exception);
        }
    };


    private void initActionBar() {
    }


    @Override
    public void onResume() {
        super.onResume();

        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onFacebookSessionStateChange(session, session.getState(), null);
        }


        uiHelper.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        uiHelper.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
        if (dialog != null) dialog.dismiss();
        Api.getHttp().cancelRequests(getActivity());
        requestLock = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

}
