package lt.lemonlabs.android.littlemail.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 27/10/13
 * Time: 00:09
 */
public class ScreenHelper {

    /**
     * Soft keyboard helpers
     */

    public static void hideSoftKeyboard(Context context, View container) {
        View focus = container.findFocus();
        if (focus != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Context context, View focus) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(focus, InputMethodManager.SHOW_IMPLICIT);
    }


    public static void hideSoftKeyboardForce(LittlemailActivity context) {
        InputMethodManager inputManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        final View focus = context.getCurrentFocus();
        if (focus != null)
            inputManager.hideSoftInputFromWindow(focus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    /**
     * Screen size and other metrics helper
     */

    private static int mWidth;
    private static int mHeight;

    public static int getScreenWidth(Activity context) {
        if (mWidth == 0) {
            calculateScreenDimensions(context);
        }
        return mWidth;
    }

    public static int getScreenHeight(Activity context) {
        if (mHeight == 0) {
            calculateScreenDimensions(context);
        }
        return mHeight;
    }


    private static void calculateScreenDimensions(Activity context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            final Point point = new Point();
            display.getSize(point);
            mWidth = point.x;
            mHeight = point.y;
        } else {
            mWidth = display.getWidth();
            mHeight = display.getHeight();
        }
    }

    public static float dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
    }

    public static float pxToDp(Context context, int px) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);

    }


    /**
     * Sets the screen brightness and returns the previous value
     *
     * @param context
     * @param brightness
     * @return
     */
    public static float setScreenBrightness(Activity context, float brightness) {
        WindowManager.LayoutParams layout = context.getWindow().getAttributes();
        final float currentBrightness = layout.screenBrightness;
        layout.screenBrightness = brightness;
        context.getWindow().setAttributes(layout);
        return currentBrightness;
    }

}
