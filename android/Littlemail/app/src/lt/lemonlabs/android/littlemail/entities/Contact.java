package lt.lemonlabs.android.littlemail.entities;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 21:41
 */
public class Contact implements Serializable {

    public final String name;
    public final String id;
    public final String url;

    public Contact(String id, String name, String url) {
        this.name = name;
        this.id = id;
        this.url = url;
    }
}
