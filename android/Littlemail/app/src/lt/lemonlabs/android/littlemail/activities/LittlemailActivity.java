package lt.lemonlabs.android.littlemail.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.common.Navigator;
import lt.lemonlabs.android.littlemail.entities.Contact;
import lt.lemonlabs.android.littlemail.fragments.ContactsFragment;
import lt.lemonlabs.android.littlemail.fragments.LittlemailFragment;
import lt.lemonlabs.android.littlemail.fragments.LoginFragment;
import lt.lemonlabs.android.littlemail.fragments.MessageFragment;

public class LittlemailActivity extends ActionBarActivity {

    private Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.main);
        navigator = new Navigator(this);

        navigator.transitionInitialFragment(LoginFragment.newInstance(), LoginFragment.TAG);
    }

    public void goToContacts() {
        navigator.transitionFragment(ContactsFragment.newInstance(), ContactsFragment.TAG);
    }

    public void goToMessage(Contact contact) {
        navigator.transitionFragment(MessageFragment.newInstance(contact), MessageFragment.TAG, R.anim.pin_code_grow_fade_in, R.anim.pin_code_shrink_fade_out);
    }


    public void goBack() {
        navigator.goBack();
    }

    @Override
    public void onBackPressed() {
        final FragmentManager manager = getSupportFragmentManager();
        final int entryCount = manager.getBackStackEntryCount();
        if (entryCount != 0) {
            final String tag = manager.getBackStackEntryAt(entryCount - 1).getName();
            LittlemailFragment fragment = (LittlemailFragment) manager.findFragmentByTag(tag);

            // Handle cases when activity should be finished
            if (fragment instanceof ContactsFragment) {
                finish();
                return;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.littlemail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
