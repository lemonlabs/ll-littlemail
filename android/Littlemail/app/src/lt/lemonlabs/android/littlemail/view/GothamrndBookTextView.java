package lt.lemonlabs.android.littlemail.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import lt.lemonlabs.android.littlemail.common.TextHelper;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 18:28
 */
public class GothamrndBookTextView extends TextView {

    public GothamrndBookTextView(Context context) {
        super(context);
    }

    public GothamrndBookTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GothamrndBookTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        // It appears that the font does not change if called earlier in the view lifecycle
        setTypeface(TextHelper.getGothamrndBook(getContext()));
    }
}
