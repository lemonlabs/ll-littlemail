package lt.lemonlabs.android.littlemail.common;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import lt.lemonlabs.android.littlemail.R;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 19:23
 */
public class DialogHelper {

    public static Dialog showTranslucentProgressDialog(final Context context, boolean isCancellable, final Runnable onCancel) {

        final Dialog progress = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        WindowManager.LayoutParams lp = progress.getWindow().getAttributes();
        progress.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        progress.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        lp.dimAmount = 0.7f;
        progress.getWindow().setAttributes(lp);

        final View content = LayoutInflater.from(context).inflate(R.layout.alert_progress, null);

        progress.setContentView(content);
        progress.setCancelable(isCancellable);

        if (isCancellable && onCancel != null) {
            progress.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    onCancel.run();
                }
            });
        }

        progress.show();

        return progress;
    }
}
