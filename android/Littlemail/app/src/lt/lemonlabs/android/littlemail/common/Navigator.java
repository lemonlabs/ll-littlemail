package lt.lemonlabs.android.littlemail.common;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;
import lt.lemonlabs.android.littlemail.fragments.LittlemailFragment;

public class Navigator {

    private static final String TAG = "NavigationHalper";
    private LittlemailActivity context;

    public Navigator(final Context context) {
        this.context = (LittlemailActivity)context;
    }

    /**
     * Show a fragment without adding it to the back stack
     *
     * @param f fragment to show
     */
    public void transitionInitialFragment(LittlemailFragment f, String tag) {
        context.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f, tag)
                .commit();
    }

    /**
     * Perform a standard fragment transaction, adding it to the backstack
     *
     * @param f fragment to show
     */
    public void transitionFragment(LittlemailFragment f) {
        context.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f)
                .addToBackStack(null)
                .commit();
    }

    public void transitionFragment(LittlemailFragment f, String tag) {
        context.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, f, tag)
                .addToBackStack(tag)
                .commit();
    }


    public void transitionFragment(LittlemailFragment f, String tag, int enterAnimationId, int exitAnimationId) {
        context.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(enterAnimationId, exitAnimationId, 0, exitAnimationId)
                .replace(R.id.container, f, tag)
                .addToBackStack(tag)
                .commit();
    }


    public void goBack() {
        FragmentManager fm = context.getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0)
            fm.popBackStackImmediate();
    }

}
