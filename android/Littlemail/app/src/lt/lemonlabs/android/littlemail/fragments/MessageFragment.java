package lt.lemonlabs.android.littlemail.fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.*;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;
import lt.lemonlabs.android.littlemail.common.*;
import lt.lemonlabs.android.littlemail.entities.Contact;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 23:38
 */
public class MessageFragment extends LittlemailFragment {

    public static final String TAG = "MessageFragment";

    private static final String ARG_CONTACT = "contact";

    private static final float BLUR_VALUE = 25f;
    private static final int COMPOSER_SIDE_PADDING_PX = 10;
    private static final int COMPOSER_EDGE_HEIGHT_PX = 5;


    private Contact contact;

    private View container;
    private EditText editText;
    private ImageView avatar;
    private View composerTop;
    private ImageLoader imageLoader;

    public static LittlemailFragment newInstance(Contact contact) {
        MessageFragment f = new MessageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_CONTACT, contact);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.message, viewGroup, false);

        Bundle args = getArguments();
        if (args != null)
            contact = (Contact) args.getSerializable(ARG_CONTACT);
        initActionBar();

        container = v.findViewById(R.id.container);
        imageLoader = ImageLoader.getInstance();
        avatar = (ImageView) v.findViewById(R.id.message_avatar);
        imageLoader.loadImage(contact.url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (loadedImage != null) {
                    avatar.setImageBitmap(loadedImage);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        loadedImage = Bitmap.createScaledBitmap(loadedImage, loadedImage.getWidth() / 4, loadedImage.getHeight() / 4, true);
                        avatar.setImageBitmap(loadedImage);
                        final BlurAnimator blurAnimator = BlurAnimator.create(getActivity(), avatar);
                        blurAnimator.setBlur(BLUR_VALUE);
                    }
                }

                avatar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }
        });

        editText = (EditText) v.findViewById(R.id.message_edit_text);
        editText.setTypeface(TextHelper.getGothamrndBook(getActivity()));

        final int convertedPadding = (int) ScreenHelper.dpToPx(getActivity(), COMPOSER_SIDE_PADDING_PX);
        final int convertedHeight = (int) ScreenHelper.dpToPx(getActivity(), COMPOSER_EDGE_HEIGHT_PX);

        // Init composer top spikes
        composerTop = v.findViewById(R.id.message_top);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) composerTop.getLayoutParams();
        params.height = convertedHeight;
        params.width = ScreenHelper.getScreenWidth(getActivity()) - (2 * convertedPadding);
        params.setMargins(convertedPadding, 0, convertedPadding, 0);

        return v;
    }


    private void sendMessage() {
        final Dialog dialog = DialogHelper.showTranslucentProgressDialog(getActivity(), false, null);

        final String text = editText.getText().toString();

        final JSONObject json = new JSONObject();
        try {
            json.put("message", text);
        } catch (JSONException e) {}

        new Api.Message(getActivity(), contact.id, json).call(getActivity(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                L.d(TAG, response);
                ((LittlemailActivity)getActivity()).goBack();

                Toast.makeText(getActivity(), "Your Little Mail has been sent!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, JSONObject errorResponse, Throwable error) {
                L.e(TAG, errorResponse);
                if (statusCode == 0) {
                    Toast.makeText(getActivity(), "No Internet connection found", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), "There was an error sending the message", Toast.LENGTH_SHORT).show();

                dialog.dismiss();
            }
        });
    }


    private void initActionBar() {
        ActionBar actionBar = ((LittlemailActivity) getActivity()).getSupportActionBar();
        if (contact != null) actionBar.setTitle(contact.name);
        else actionBar.setTitle(R.string.no_name);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.show();
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        editText.requestFocus();
        ScreenHelper.showSoftKeyboard(getActivity(), editText);
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Api.getHttp().cancelRequests(getActivity());
        ScreenHelper.hideSoftKeyboard(getActivity(), container);
        ((LittlemailActivity)getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.message, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                ((LittlemailActivity) getActivity()).goBack();
                return true;
            case R.id.action_send:
                sendMessage();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
