package lt.lemonlabs.android.littlemail.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.*;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.loopj.android.http.JsonHttpResponseHandler;
import lt.lemonlabs.android.littlemail.R;
import lt.lemonlabs.android.littlemail.activities.LittlemailActivity;
import lt.lemonlabs.android.littlemail.adapters.ContactsAdapter;
import lt.lemonlabs.android.littlemail.common.Api;
import lt.lemonlabs.android.littlemail.common.FBHelper;
import lt.lemonlabs.android.littlemail.common.L;
import lt.lemonlabs.android.littlemail.entities.Contact;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 19:05
 */
public class ContactsFragment extends LittlemailFragment {

    public static final String TAG = "ContactsFragment";

    private ProgressBar progressBar;
    private ListView contactsList;
    private ContactsAdapter adapter;

    public static LittlemailFragment newInstance() {
        ContactsFragment f = new ContactsFragment();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.contacts, viewGroup, false);
        initActionBar();

        contactsList = (ListView) v.findViewById(R.id.contacts_list);
        contactsList.setItemsCanFocus(true);
        progressBar = (ProgressBar) v.findViewById(R.id.contacts_pb);
        adapter = new ContactsAdapter(getActivity());


        loadContacts();

        return v;
    }


    private void loadContacts() {
        new Api.Friends(getActivity()).call(getActivity(), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, JSONObject response) {
                L.d(TAG, response);

                final JSONArray jsonArray = response.optJSONArray("friends");
                if (jsonArray != null) {
                    final ArrayList<Contact> contacts = new ArrayList<Contact>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        final JSONObject contact = jsonArray.optJSONObject(i);
                        final String id = contact.optString("id", "0");
                        final String name = contact.optString("name", "");
                        final String url = contact.optString("picture", "");

                        contacts.add(new Contact(id, name, url));
                    }

                    adapter.setItems(contacts);
                    contactsList.setAdapter(adapter);
                }



                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(int statusCode, JSONObject errorResponse, Throwable error) {
                L.e(TAG, errorResponse);
                progressBar.setVisibility(View.GONE);
                if (statusCode == 0) {
                    Toast.makeText(getActivity(), "No Internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    private void initActionBar() {
        ActionBar actionBar = ((LittlemailActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle(R.string.app_name);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.show();
        setHasOptionsMenu(true);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Api.getHttp().cancelRequests(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.contacts, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
//            case R.id.action_search:
//                return true;
            case R.id.action_refresh:
                loadContacts();
                return true;
            case R.id.action_settings:
                return true;
            case R.id.action_logout:
                FBHelper.logout(getActivity());
                getActivity().finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
