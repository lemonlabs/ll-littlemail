package lt.lemonlabs.android.littlemail.common;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.widget.ImageView;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 27/10/13
 * Time: 00:28
 */
public class BlurAnimator implements ValueAnimator.AnimatorUpdateListener {


    private Context mContext;
    private ImageView mImageView;

    private ValueAnimator mValueAnimator;

    private Bitmap mBitmapIn;
    private Bitmap mBitmapOut;

    private RenderScript mRS;
    private ScriptIntrinsicBlur mBlurScript;

    private Allocation mInAllocation;
    private Allocation mOutAllocation;

    public static BlurAnimator create(Context context, ImageView imageView) {
        return new BlurAnimator(context, imageView);
    }

    private BlurAnimator(Context context, ImageView imageView) {
        mBitmapIn = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        mBitmapOut = mBitmapIn.copy(mBitmapIn.getConfig(), true);
        mContext = context;
        mImageView = imageView;
        mRS = RenderScript.create(mContext);
        mBlurScript = ScriptIntrinsicBlur.create(mRS, Element.U8_4(mRS));
    }

    public BlurAnimator values(float from, float to) {
        mValueAnimator = ValueAnimator.ofFloat(from, to);
        mValueAnimator.addUpdateListener(this);
        return this;
    }

    public BlurAnimator duration(long duration) {
        mValueAnimator.setDuration(duration);
        return this;
    }

    public BlurAnimator values(ValueAnimator valueAnimator) {
        mValueAnimator = valueAnimator;
        mValueAnimator.addUpdateListener(this);
        return this;
    }

    private Bitmap getBlurredImage(Bitmap in, float value) {
        mInAllocation = Allocation.createFromBitmap(mRS, in,
                Allocation.MipmapControl.MIPMAP_NONE,
                Allocation.USAGE_SCRIPT);
        mOutAllocation = Allocation.createTyped(mRS, mInAllocation.getType());
        mBlurScript.setInput(mInAllocation);
        mBlurScript.setRadius(value);
        mBlurScript.forEach(mOutAllocation);
        mOutAllocation.copyTo(mBitmapOut);
        mInAllocation.destroy();
        mOutAllocation.destroy();
        return mBitmapOut;
    }

    public void setBlur(float value) {
        mImageView.setImageBitmap(getBlurredImage(mBitmapIn, value));
    }

    public void start() {
        if (mValueAnimator != null) mValueAnimator.start();
    }

    public void cancel() {
        if (mValueAnimator != null) mValueAnimator.cancel();
    }

    public void destroy() {
        mRS.destroy();
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        float value = (Float) animation.getAnimatedValue();
        mImageView.setImageBitmap(getBlurredImage(mBitmapIn, value));
    }
}
