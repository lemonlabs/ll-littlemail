package lt.lemonlabs.android.littlemail.common;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 21:01
 */
public class AppSettings {


    private static final String PREFS_NAME = "app_settings";
    private static final String ARG_FB_TOKEN = "fb_token";
    private static final String ARG_APP_TOKEN = "app_token";

    private static AppSettings instance;

    public static AppSettings getInstance(Context context) {
        if (instance == null) instance = new AppSettings(context);
        return instance;
    }

    private Context context;
    private SharedPreferences prefs;

    private AppSettings(Context context) {
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }


    public void setFacebookToken(String token) {
        prefs.edit().putString(ARG_FB_TOKEN, token).commit();
    }

    public String getFacebookToken() {
        return prefs.getString(ARG_FB_TOKEN, "");
    }

    public void setAppToken(String token) {
        prefs.edit().putString(ARG_APP_TOKEN, token).commit();
    }

    public String getAppToken() {
        return prefs.getString(ARG_APP_TOKEN, "");
    }

}
