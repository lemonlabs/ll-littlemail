package lt.lemonlabs.android.littlemail.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import lt.lemonlabs.android.littlemail.common.L;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 16:22
 */
public class LittlemailFragment extends Fragment {

    public static final boolean IS_PRODUCTION = false;

    private static final String TAG = "PayseraFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onCreate");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onCreateView");
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onActivityCreated");
        }

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onResume");
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onPause");
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onDestroyView");
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onDestroy");
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onStart");
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onStop");
        }
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onAttach");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (!IS_PRODUCTION) {
            L.v((this).getClass().getSimpleName(), "onDetach");
        }
    }

}
