package lt.lemonlabs.android.littlemail.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 17:48
 */
public class TextHelper {
    /**
     * TextView and Button typeface helpers
     */

    private static Typeface gothamrnd;
    private static Typeface gothamrndBook;

    public static Typeface getGothamrnd(Context context) {
        if (gothamrnd == null) {
            gothamrnd = Typeface.createFromAsset(context.getAssets(), "fonts/gothamrnd_medium.otf");
        }
        return gothamrnd;
    }

    public static Typeface getGothamrndBook(Context context) {
        if (gothamrndBook == null) {
            gothamrndBook = Typeface.createFromAsset(context.getAssets(), "fonts/gothamrnd_book.otf");
        }
        return gothamrndBook;
    }
}
