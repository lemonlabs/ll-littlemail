package lt.lemonlabs.android.littlemail.common;

import android.util.Log;
import lt.lemonlabs.android.littlemail.fragments.LittlemailFragment;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 16:24
 */
public class L {
    private static final String PREFIX = "lt.lemonlabs.android.littlemail/";

    public static void v(String tag, String message) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.v(PREFIX.concat(tag), message);
        }
    }

    public static void v(String tag, boolean b) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.v(PREFIX.concat(tag), String.valueOf(b));
        }
    }

    public static void v(String tag, int i) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.v(PREFIX.concat(tag), String.valueOf(i));
        }
    }

    public static void v(String tag, JSONObject json) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.v(PREFIX.concat(tag), json.toString());
        }
    }

    public static void d(String tag, String message) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.d(PREFIX.concat(tag), message);
        }
    }

    public static void d(String tag, boolean b) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.v(PREFIX.concat(tag), String.valueOf(b));
        }
    }

    public static void d(String tag, JSONObject json) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.d(PREFIX.concat(tag), json.toString());
        }
    }

    public static void e(String tag, String message) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.e(PREFIX.concat(tag), message);
        }
    }

    public static void e(String tag, String message, Throwable error) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.e(PREFIX.concat(tag), message, error);
        }
    }

    public static void e(String tag, JSONObject json) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            if (json != null) Log.e(PREFIX.concat(tag), json.toString());
            else Log.e(PREFIX.concat(tag), "Invalid JSON response");
        }
    }

    public static void i(String tag, String message) {
        if (!LittlemailFragment.IS_PRODUCTION) {
            Log.i(PREFIX.concat(tag), message);
        }
    }
}
