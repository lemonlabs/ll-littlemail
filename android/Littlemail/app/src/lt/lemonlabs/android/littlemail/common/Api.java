package lt.lemonlabs.android.littlemail.common;

import android.content.Context;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import lt.lemonlabs.android.littlemail.R;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 20:47
 */
public class Api {

    private static final String TAG = "API";

    // HTTP client parameters
    private static final int MAX_RETRIES = 3;
    private static final int RETRY_SLEEP_TIME_MILLIS = 500;
    private static final int MAX_API_THREADS = 4;
    private static final int TIMEOUT = 20000;

    private static AsyncHttpClient http;

    public static AsyncHttpClient getHttp() {
        if (http == null) {
            http = new AsyncHttpClient(MAX_RETRIES, RETRY_SLEEP_TIME_MILLIS);
            http.setTimeout(TIMEOUT);
            http.setThreadPool((ThreadPoolExecutor) Executors.newFixedThreadPool(MAX_API_THREADS));
        }
        return http;
    }


    /**
     * Core request class. Divided into two main calls: one for client session related request, another
     * for user session requests.
     */
    private static class AuthorizedRequest {

        private RequestParams params = new RequestParams();
        private StringEntity jsonParams;
        private boolean useToken;

        private AuthorizedRequest(Context context) {
        }

        /**
         * Execute the request
         */
        public void execute(final Context context, final String url, final BaseRequest.HttpMethod method, final JsonHttpResponseHandler handler) {

//            final Header[] authHeader = new Header[1];
//            if (useToken) authHeader[0] = new BasicHeader("Authorization", AppSettings.getInstance(context).getFacebookToken());

            final String contentType = "application/json";

            final String finalUrl = context.getString(R.string.api) + url;

            L.d(TAG, finalUrl + " " + method);

            switch (method) {
                case GET:
                    getHttp().get(context, finalUrl, params, handler);
                    break;
                case POST:
                    getHttp().post(context, finalUrl, jsonParams, contentType, handler);
                    break;
            }

        }


        public void jsonBody(JSONObject json) {
            try {
                jsonParams = new StringEntity(json.toString());
            } catch (Exception e) {
                L.e(TAG, "", e);
            }
        }

        public void useToken() {
            useToken = true;
        }

    }


    public abstract static class BaseRequest {

        protected AuthorizedRequest r;

        protected enum HttpMethod {GET, POST, PUT, DELETE};

        public BaseRequest(Context context) { r = new AuthorizedRequest(context); }

        public void call(final Context initiator, final JsonHttpResponseHandler handler) {
            r.execute(initiator, getUrl(), getMethod(), handler);
        }

        protected abstract HttpMethod getMethod();
        protected abstract String getUrl();

    }


    public static class OAuth extends BaseRequest {

        public OAuth(Context context, JSONObject json) {
            super(context);
            r.jsonBody(json);
        }

        @Override
        protected HttpMethod getMethod() {
            return HttpMethod.POST;
        }

        @Override
        protected String getUrl() {
            return "/oauth";
        }
    }

    public static class Friends extends BaseRequest {

        public Friends(Context context) {
            super(context);
            r.useToken();
        }

        @Override
        protected HttpMethod getMethod() {
            return HttpMethod.GET;
        }

        @Override
        protected String getUrl() {
            return "/friends";
        }
    }


    public static class Message extends BaseRequest {

        final String id;

        public Message(Context context, String id, JSONObject json) {
            super(context);
            this.id = id;
            r.jsonBody(json);
        }

        @Override
        protected HttpMethod getMethod() {
            return HttpMethod.POST;
        }

        @Override
        protected String getUrl() {
            return String.format(Locale.US, "/friends/%s/message", id);
        }
    }

}