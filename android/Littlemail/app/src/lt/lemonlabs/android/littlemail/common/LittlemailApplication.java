package lt.lemonlabs.android.littlemail.common;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

/**
 * Created with IntelliJ IDEA.
 * User: balysv
 * Date: 26/10/13
 * Time: 22:35
 */
public class LittlemailApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        initImageLoader();
        mContext = this;

    }


    /**
     * Make sure that locale is overwritten when screen orientation or other configurations changes.
     *
     * @param newConfig The new device configuration
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Initialize nostra13 UniversalImageLoader
     * Using a LRU cache with size of 1/8 of MAX device memory
     */
    private void initImageLoader() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 8;

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .imageScaleType(ImageScaleType.NONE)
                .displayer(new SimpleBitmapDisplayer())
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .threadPoolSize(3)
                .memoryCache(new LruMemoryCache(cacheSize))
                .build();
        ImageLoader.getInstance().init(config);
    }

    public static Context getAppContext() {
        return mContext;
    }
}
