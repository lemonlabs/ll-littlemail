//
//  Friend.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "Friend.h"

@implementation Friend

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id" : @"identifier", @"picture" : @"imageUrl" }];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Friend: %@ (%@)", self.name, self.identifier];
}

@end




@implementation Friends

+ (JSONKeyMapper *)keyMapper
{
    return [JSONKeyMapper mapperFromUnderscoreCaseToCamelCase];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Friends: %@ ", self.friends];
}

@end