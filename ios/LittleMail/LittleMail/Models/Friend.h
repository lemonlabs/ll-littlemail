//
//  Friend.h
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

@import Foundation;
#import <JSONModel.h>

@protocol Friend @end;
@interface Friend : JSONModel

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString <Optional> *imageUrl;

@end



@interface Friends : JSONModel
@property (nonatomic, strong) NSArray <Friend> *friends;
@end