//
//  ContactsController.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "ContactsController.h"
#import "APIManager.h"
#import "AppDelegate.h"
#import "Friend.h"
#import "FriendCell.h"
#import "WriteController.h"
#import <SVProgressHUD.h>

@interface ContactsController ()
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, strong) Friend *selectedFriend;
- (IBAction)settingsTapped:(UIBarButtonItem *)sender;
@end

@implementation ContactsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Little Mail";
	
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    
    NSDictionary *appearance = @{NSFontAttributeName : [UIFont fontWithName:@"GothamRounded-Book" size:15]};
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:appearance forState:UIControlStateNormal];
    [self.navigationItem.rightBarButtonItem setTitlePositionAdjustment:UIOffsetMake(0, 0.5) forBarMetrics:UIBarMetricsDefault];
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *facebookToken = app.session.accessTokenData.accessToken;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [[APIManager sharedClient] authWithFacebookToken:facebookToken completion:^(NSError *error) {
        
        if (!error) {
            [[APIManager sharedClient] getFriends:^(Friends *data, NSError *error) {
                [SVProgressHUD dismiss];
                self.friends = data.friends;
                [self.tableView reloadData];
            }];
        } else {
            [SVProgressHUD dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upppsss" message:error.localizedDescription delegate:self cancelButtonTitle:@"Try again" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.friends count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FriendCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"FriendCell"];
    
    Friend *friend = self.friends[indexPath.row];
    cell.friend = friend;
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedFriend = self.friends[indexPath.row];
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self performSegueWithIdentifier:@"WriteNewMail" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56.0;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.session closeAndClearTokenInformation];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"WriteNewMail"]) {
        WriteController *controller = segue.destinationViewController;
        controller.friend = self.selectedFriend;
    }
}

- (IBAction)settingsTapped:(UIBarButtonItem *)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [app.session closeAndClearTokenInformation];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
