//
//  WriteController.h
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@interface WriteController : UIViewController

@property (nonatomic, strong) Friend *friend;

@end
