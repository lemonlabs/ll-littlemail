//
//  WriteController.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "WriteController.h"
#import "Friend.h"
#import <UIImageView+AFNetworking.h>
#import "APIManager.h"
#import <UIImageView+LBBlurredImage.h>
#import <EAMTextView.h>
#import <SVProgressHUD.h>
#import <BOSImageResizeOperation.h>

@interface WriteController () <EAMTextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *blurImageView;
@property (weak, nonatomic) IBOutlet EAMTextView *textView;
@property (weak, nonatomic) IBOutlet UIView *paperView;
@property (weak, nonatomic) IBOutlet UIImageView *paperDecorImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *sendMailButton;

- (IBAction)sendMailButtonTapped:(UIButton *)sender;
@end

@implementation WriteController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.friend.name;
    
    self.textView.tintColor = [UIColor colorWithRed:0.984 green:0.780 blue:0.161 alpha:1.000];
    
    NSURL *imageUrl = nil;
    
    if (!self.friend.imageUrl) {
        imageUrl = [NSURL URLWithString:[[APIManager sharedClient] currentUserImageUrl]];
    } else {
        imageUrl = [NSURL URLWithString:self.friend.imageUrl];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:imageUrl];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.blurImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        BOSImageResizeOperation *op = [[BOSImageResizeOperation alloc] initWithImage:image];
        [op resizeToFitWithinSize:CGSizeMake(200.0, 200.0)];
        [op start];
        
        self.blurImageView.image = op.result;
        self.blurImageView.alpha = 0.0;
        
        [self.blurImageView setImageToBlur:op.result blurRadius:10 completionBlock:^(NSError *error) {
            [UIView animateWithDuration:0.3 animations:^{
                self.blurImageView.alpha = 1.0;
            }];
        }];
        
    } failure:nil];
    
    self.sendMailButton.titleLabel.font = [UIFont fontWithName:@"GothamRounded-Medium" size:17];
    
    self.textView.delegate = self;
    self.textView.autoresizesVertically = YES;
    self.textView.minimumHeight = 35.0f;
    self.textView.maximumHeight = 200.0f;
    self.textView.placeholder = @"Type something...";
    self.textView.font = [UIFont fontWithName:@"GothamRounded-Book" size:17];
    [self.textView becomeFirstResponder];
}

#pragma mark - EAMTextViewDelegate

- (void)textView:(EAMTextView *)textView willChangeFromHeight:(CGFloat)oldHeight toHeight:(CGFloat)newHeight
{
    CGRect frame = self.paperView.frame;
    CGFloat difference = newHeight - oldHeight;
    frame.size.height += difference;
    frame.origin.y -= difference;
    
    
    CGRect f = self.paperDecorImage.frame;
    f.origin.y = frame.origin.y - f.size.height;
    
    self.paperView.frame = frame;
    self.paperDecorImage.frame = f;
}

- (void)textView:(EAMTextView *)textView didChangeFromHeight:(CGFloat)oldHeight toHeight:(CGFloat)newHeight
{
    NSLog(@"Finished height change animation from height %f to height %f", oldHeight, newHeight);
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    //NSLog(@"Text did change");
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    //NSLog(@"Ended editing");
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //NSLog(@"Began editing");
}

- (IBAction)sendMailButtonTapped:(UIButton *)sender
{
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
    [[APIManager sharedClient] sendMessage:self.textView.text toFriend:self.friend completion:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        if (!error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your Little Mail has been sent!" message:nil delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
            [alert show];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upppsss" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Try again" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
