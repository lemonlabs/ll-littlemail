//
//  LoginController.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "LoginController.h"
#import <FacebookSDK.h>
#import "AppDelegate.h"
#import <SVProgressHUD.h>
#import "APIManager.h"

@interface LoginController ()
@property (nonatomic, weak) IBOutlet UILabel *infoTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *infoTextLabel;
@property (nonatomic, weak) IBOutlet UIButton *loginButton;
@property (nonatomic, weak) IBOutlet UIButton *learnMoreButton;

- (IBAction)loginButtonTapped:(UIButton *)sender;
- (IBAction)learnMoreButtonTapped:(UIButton *)sender;
@end

@implementation LoginController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.infoTitleLabel.font        = [UIFont fontWithName:@"GothamRounded-Bold" size:14];
    self.infoTextLabel.font         = [UIFont fontWithName:@"GothamRounded-Book" size:14];
    self.infoTitleLabel.textColor   = [UIColor colorWithRed:0.337 green:0.263 blue:0.027 alpha:1.000];
    self.infoTextLabel.textColor    = [UIColor colorWithRed:0.337 green:0.263 blue:0.027 alpha:1.000];
    
    self.loginButton.titleLabel.font     = [UIFont fontWithName:@"GothamRounded-Book" size:14];
    self.learnMoreButton.titleLabel.font = [UIFont fontWithName:@"GothamRounded-Book" size:14];
    
    [self updateView];
	
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!app.session.isOpen) {
        app.session = [[FBSession alloc] init];
        if (app.session.state == FBSessionStateCreatedTokenLoaded) {
            [app.session openWithCompletionHandler:^(FBSession *session,
                                                             FBSessionState status,
                                                             NSError *error) {
                [self updateView];
            }];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (IBAction)loginButtonTapped:(UIButton *)sender
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (app.session.isOpen) {
        // Logout if session is open
        [app.session closeAndClearTokenInformation];
        [[APIManager sharedClient] setCurrentUser:nil];
        
        [self updateView];
        
    } else {
        // Login
        if (app.session.state != FBSessionStateCreated) {
            app.session = [[FBSession alloc] init];
        }
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        [app.session openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            [SVProgressHUD dismiss];
            [self updateView];
            
            if (error) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upppsss" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Try again" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

- (IBAction)learnMoreButtonTapped:(UIButton *)sender {
}

- (void)updateView
{
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (app.session.isOpen) {
        //self.loginButton.hidden = YES;
        [self.loginButton setTitle:@"Logout" forState:UIControlStateNormal];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        FBRequest *request = [[FBRequest alloc] initWithSession:app.session graphPath:@"me"];
        [request startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary <FBGraphUser> *user, NSError *error) {
            [SVProgressHUD dismiss];
            [[APIManager sharedClient] setCurrentUser:user];
            
            if (!error) {
                [self performSegueWithIdentifier:@"ShowContactsScreen" sender:self];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Upppsss" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Try again" otherButtonTitles:nil];
                [alert show];
            }
        }];
        
    } else {
        //self.loginButton.hidden = NO;
        [self.loginButton setTitle:@"Connect with Facebook" forState:UIControlStateNormal];
    }
}

@end
