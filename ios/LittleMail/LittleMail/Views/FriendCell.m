//
//  FriendCell.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "FriendCell.h"
#import "Friend.h"
#import <UIImageView+AFNetworking.h>
#import "APIManager.h"

@interface FriendCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIImageView *avatarCover;
@end

@implementation FriendCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.nameLabel.font = [UIFont fontWithName:@"GothamRounded-Book" size:17];
}

- (void)setFriend:(Friend *)friend
{
    _friend = friend;
    
    self.nameLabel.text = friend.name;
    if (!friend.imageUrl) {
        [self.avatarImage setImageWithURL:[NSURL URLWithString:[[APIManager sharedClient] currentUserImageUrl]]];
    } else {
        [self.avatarImage setImageWithURL:[NSURL URLWithString:friend.imageUrl]];
    }
    self.avatarCover.image = [UIImage imageNamed:@"friendsAvatarCover1"];
}

@end
