//
//  APIManager.h
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

@import Foundation;
#import "AFHTTPSessionManager.h"

@class Friends, Friend;

typedef void (^AuthBlock)(NSError *error);
typedef void (^FriendsBlock)(Friends *data, NSError *error);
typedef void (^SendBlock)(NSError *error);

@interface APIManager : AFHTTPSessionManager

@property (nonatomic, strong) NSDictionary *currentUser;
@property (nonatomic, readonly) NSString *currentUserImageUrl;

+ (instancetype)sharedClient;

- (void)authWithFacebookToken:(NSString *)token completion:(AuthBlock)block;
- (void)getFriends:(FriendsBlock)block;
- (void)sendMessage:(NSString *)message toFriend:(Friend *)friend completion:(SendBlock)block;

@end
