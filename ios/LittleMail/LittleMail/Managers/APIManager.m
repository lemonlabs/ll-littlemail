//
//  APIManager.m
//  LittleMail
//
//  Created by Marius Kažemėkaitis on 2013-10-26.
//  Copyright (c) 2013 Marius Kažemėkaitis. All rights reserved.
//

#import "APIManager.h"
#import "Friend.h"

static NSString * const kAPIBaseURLString   = @"http://lemonlabs.lt:3000";

@implementation APIManager

+ (instancetype)sharedClient
{
    static APIManager *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIManager alloc] initWithBaseURL:[NSURL URLWithString:kAPIBaseURLString]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    
    return self;
}

- (NSString *)currentUserImageUrl
{
    if (self.currentUser && self.currentUser[@"username"]) {
        return [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", self.currentUser[@"username"]];
    }
    
    return nil;
}

- (void)authWithFacebookToken:(NSString *)token completion:(AuthBlock)block
{
    [self.requestSerializer clearAuthorizationHeader];
    
    NSDictionary *params = @{@"facebook_token": token};
    [self POST:@"auth" parameters:params success:^(NSURLSessionDataTask * __unused task, id JSON) {
        
        [self.requestSerializer setAuthorizationHeaderFieldWithToken:JSON[@"token"]];
        //[self.requestSerializer setValue:JSON[@"token"] forHTTPHeaderField:@"Authorization"];
        
        if (block) {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        NSLog(@"%@", error);
        if (block) {
            block(error);
        }
    }];
}

- (void)getFriends:(FriendsBlock)block
{
    [self GET:@"friends" parameters:nil success:^(NSURLSessionDataTask * __unused task, id JSON) {
        //NSLog(@"%@", task.originalRequest.allHTTPHeaderFields);
        
        NSError *error = nil;
        Friends *data = [[Friends alloc] initWithDictionary:JSON error:&error];
        if (error) {
            NSLog(@"JSON error: %@", error);
        }
        
        if (block) {
            block(data, nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        NSLog(@"%@", error);
        if (block) {
            block(nil, error);
        }
    }];
}

- (void)sendMessage:(NSString *)message toFriend:(Friend *)friend completion:(SendBlock)block
{
    NSString *path = [NSString stringWithFormat:@"friends/%@/message", friend.identifier];
    NSDictionary *params = @{@"message": message};
    
    [self POST:path parameters:params success:^(NSURLSessionDataTask * __unused task, id JSON) {
        NSLog(@"--%@", task.originalRequest.allHTTPHeaderFields);
        NSLog(@"JSON: %@", JSON);
        
        if (block) {
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *__unused task, NSError *error) {
        NSLog(@"%@", error);
        if (block) {
            block(error);
        }
    }];
}

@end
