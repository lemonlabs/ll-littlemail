EAMTextView
===========

EAMTextView is UITextView subclass that adds placeholder and vertical autoresizing support.
